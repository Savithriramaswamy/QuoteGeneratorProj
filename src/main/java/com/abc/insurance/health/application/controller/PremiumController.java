package com.abc.insurance.health.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.abc.insurance.health.application.model.PremiumRequest;
import com.abc.insurance.health.application.model.PremiumResponse;
import com.abc.insurance.health.application.service.QuoteService;

@Controller
public class PremiumController {
	@Autowired
	private QuoteService service;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/calculatePremium" ,method=RequestMethod.POST)
	public String calculateTotalPremium(
			@ModelAttribute("premiumForm") PremiumRequest premiumForm, Model model) {
		double totalPremiumAmount = 0;
		PremiumResponse response = new PremiumResponse();
		try {
			logger.info("Input Request :" + premiumForm.toString());
			totalPremiumAmount = service.getPremiumAmount(premiumForm);
			response.setAmount(totalPremiumAmount);
			response.setName(premiumForm.getName());
		} catch (Exception e) {
			model.addAttribute("errorMessage","Please provide all the details");
			logger.error("Error :" + e.getMessage());
			return "getQuote";
		}
		model.addAttribute("response", response);
		logger.info("Response of calculateTotalPremium() "
				+ response.toString());
		return "getQuote";
	}
	@RequestMapping(value = "/calculatePremium" ,method=RequestMethod.GET)
	public String registerCommandName(Model model) {
	
		model.addAttribute("premiumForm", new PremiumRequest());
		
		return "getQuote";
	}
	
	
	@RequestMapping(value = "/underConstruction")
	public String underConstruction() {
		
		return "underConstruction";
	}
	
}
