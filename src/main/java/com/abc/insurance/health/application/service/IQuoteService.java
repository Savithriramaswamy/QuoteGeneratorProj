package com.abc.insurance.health.application.service;

import org.springframework.stereotype.Service;

import com.abc.insurance.health.application.model.PremiumRequest;

@Service
public interface IQuoteService {
	public double getPremiumAmount(PremiumRequest form) ;
	}
